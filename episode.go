package tmdb

import "gitlab.com/bazzz/dates"

type Episode struct {
	ID        int        `json:"id"`
	Premiered dates.Date `json:"air_date"`
	Title     string     `json:"name"`
	Overview  string     `json:"overview"`
	Season    int        `json:"season_number"`
	Number    int        `json:"episode_number"`
	Image     string     `json:"still_path"`
	Rating    float64    `json:"vote_average"`
	Votes     int        `json:"vote_count"`
	// Crew (maybe later) `json:"crew"`
	// GuestStars (maybe later) `json:"guest_stars"`
}
