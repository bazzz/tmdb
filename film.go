package tmdb

import "gitlab.com/bazzz/dates"

type filmSearchResult struct {
	Page  int            `json:"page"`
	Films []searchedFilm `json:"results"`
}

// Film represents a film.
type Film struct {
	ID               int        `json:"id"`
	Title            string     `json:"title"`
	Premiered        dates.Date `json:"release_date"`
	Overview         string     `json:"overview"`
	OriginalTitle    string     `json:"original_title"`
	OriginalLanguage string     `json:"original_language"`
	Rating           float64    `json:"vote_average"`
	Popularity       float64    `json:"popularity"`
	Votes            int        `json:"vote_count"`
	Poster           string     `json:"poster_path"`
	Backdrop         string     `json:"backdrop_path"`
	Genres           []string
}

type searchedFilm struct {
	Film
	GenreIDs []int `json:"genre_ids"`
}

func newFilmFromSearched(input searchedFilm, genres map[int]string) *Film {
	f := input.Film
	for _, genreid := range input.GenreIDs {
		genre := genres[genreid]
		if genre != "" {
			f.Genres = append(f.Genres, genre)
		}
	}
	return &f
}

type detailedFilm struct {
	Film
	GenreObjects []genre `json:"genres"`
}

func newFilmFromDetailed(input detailedFilm, genres map[int]string) *Film {
	f := input.Film
	for _, genreObject := range input.GenreObjects {
		genre := genres[genreObject.ID]
		if genre != "" {
			f.Genres = append(f.Genres, genre)
		}
	}
	return &f
}
