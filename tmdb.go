package tmdb

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/ferhatelmas/levenshtein"
	"gitlab.com/bazzz/dates"
)

const (
	baseURL  = "http://api.themoviedb.org/3"
	imageURL = "https://image.tmdb.org/t/p/original"
)

// New returns a TMDB client with the specified apikey.
func New(apiKey string) (*Client, error) {
	client := &Client{
		ApiKey: apiKey,
	}

	showgenres, err := client.getGenres("tv")
	if err != nil {
		return nil, err
	}
	client.showGenres = showgenres

	filmgenres, err := client.getGenres("tv")
	if err != nil {
		return nil, err
	}
	client.filmGenres = filmgenres

	return client, nil
}

// Client represents a TMDB client.
type Client struct {
	ApiKey     string
	filmGenres map[int]string
	showGenres map[int]string
}

func call(uri string) ([]byte, error) {
	response, err := http.Get(uri)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return nil, errors.New("http status: " + response.Status)
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// SearchFilm returns a Film by provided title, year, and optional ISO3166-1 alpha2 language code.
func (c *Client) SearchFilm(title string, year int, language string) (*Film, error) {
	uri := baseURL + "/search/movie"
	uri += "?api_key=" + c.ApiKey
	if len(language) == 2 {
		uri += "&language=" + language
	}
	uri += "&query=" + url.QueryEscape(title)
	uri += "&year=" + strconv.Itoa(year)
	data, err := call(uri)
	if err != nil {
		return nil, err
	}

	filmResult := filmSearchResult{}
	err = json.Unmarshal(data, &filmResult)
	if err != nil {
		return nil, err
	}
	if len(filmResult.Films) == 0 {
		return nil, nil
	}
	film := c.bestFilmMatch(title, year, filmResult)
	if film.Poster != "" {
		film.Poster = imageURL + film.Poster
	}
	if film.Backdrop != "" {
		film.Backdrop = imageURL + film.Backdrop
	}
	return film, nil
}

// GetFilm returns a Film by provided TMDB id, and optional ISO3166-1 alpha2 language code.
func (c *Client) GetFilm(id int, language string) (*Film, error) {
	uri := baseURL + "/movie/"
	uri += strconv.Itoa(id)
	uri += "?api_key=" + c.ApiKey
	if len(language) == 2 {
		uri += "&language=" + language
	}
	data, err := call(uri)
	if err != nil {
		return nil, err
	}
	detailedFilm := detailedFilm{}
	err = json.Unmarshal(data, &detailedFilm)
	if err != nil {
		return nil, err
	}
	film := newFilmFromDetailed(detailedFilm, c.filmGenres)
	if film.Poster != "" {
		film.Poster = imageURL + film.Poster
	}
	if film.Backdrop != "" {
		film.Backdrop = imageURL + film.Backdrop
	}
	return film, nil
}

// SearchShow returns a Show by provided title, and optional ISO3166-1 alpha2 language code.
func (c *Client) SearchShow(title string, language string) (*Show, error) {
	uri := baseURL + "/search/tv"
	uri += "?api_key=" + c.ApiKey
	if len(language) == 2 {
		uri += "&language=" + language
	}
	uri += "&query=" + url.QueryEscape(title)
	data, err := call(uri)
	if err != nil {
		return nil, err
	}
	showResult := showSearchResult{}
	err = json.Unmarshal(data, &showResult)
	if err != nil {
		return nil, err
	}
	if len(showResult.Shows) == 0 {
		return nil, nil
	}
	show := c.bestShowMatch(title, showResult)
	if show.Poster != "" {
		show.Poster = imageURL + show.Poster
	}
	if show.Backdrop != "" {
		show.Backdrop = imageURL + show.Backdrop
	}
	return show, nil
}

// GetShow returns a Show by provided TMDB id, and optional ISO3166-1 alpha2 language code.
func (c *Client) GetShow(id int, language string) (*Show, error) {
	uri := baseURL + "/tv/"
	uri += strconv.Itoa(id)
	uri += "?api_key=" + c.ApiKey
	if len(language) == 2 {
		uri += "&language=" + language
	}
	data, err := call(uri)
	if err != nil {
		return nil, err
	}
	detailedShow := detailedShow{}
	err = json.Unmarshal(data, &detailedShow)
	if err != nil {
		return nil, err
	}
	show := newShowFromDetailed(detailedShow, c.showGenres)
	if show.Poster != "" {
		show.Poster = imageURL + show.Poster
	}
	if show.Backdrop != "" {
		show.Backdrop = imageURL + show.Backdrop
	}
	return show, nil
}

// GetEpisode returns an Episode with season and number for the provided TMDB show ID, and optional ISO3166-1 alpha2 language code.
func (c *Client) GetEpisode(showID int, season int, number int, language string) (*Episode, error) {
	uri := baseURL + "/tv/"
	uri += strconv.Itoa(showID)
	uri += "/season/"
	uri += strconv.Itoa(season)
	uri += "/episode/"
	uri += strconv.Itoa(number)
	uri += "?api_key=" + c.ApiKey
	if len(language) == 2 {
		uri += "&language=" + language
	}
	data, err := call(uri)
	if err != nil {
		return nil, err
	}
	episode := Episode{}
	err = json.Unmarshal(data, &episode)
	if err != nil {
		return nil, err
	}
	if episode.Image != "" {
		episode.Image = imageURL + episode.Image
	}
	return &episode, nil
}

func (c *Client) getGenres(videotype string) (map[int]string, error) {
	genres := make(map[int]string)
	uri := baseURL + "/genre/" + videotype + "/list"
	uri += "?api_key=" + c.ApiKey
	data, err := call(uri)
	if err != nil {
		return genres, err
	}
	genreResult := genreResult{}
	err = json.Unmarshal(data, &genreResult)
	if err != nil {
		return genres, err
	}
	for _, genre := range genreResult.Genres {
		genres[genre.ID] = genre.Name
	}
	return genres, err
}

func (c *Client) bestFilmMatch(title string, year int, result filmSearchResult) *Film {
	if len(result.Films) == 1 {
		return newFilmFromSearched(result.Films[0], c.filmGenres)
	}
	candidates := make([]searchedFilm, 0)
	for _, film := range result.Films {
		if film.Premiered.Year() == year {
			candidates = append(candidates, film)
		}
	}
	if len(candidates) == 1 {
		return newFilmFromSearched(candidates[0], c.filmGenres)
	}
	if len(candidates) > 1 {
		tuples := make(map[dates.Date]string)
		for _, film := range candidates {
			if _, ok := tuples[film.Premiered]; ok {
				continue // do not overwrite an existing candidate with the same Premiered date.
			}
			tuples[film.Premiered] = cleanTitle(film.Title)
		}
		premiered := selectByStringDistance(title, tuples)
		for _, candidate := range candidates {
			if candidate.Premiered.String() == premiered.String() {
				return newFilmFromSearched(candidate, c.filmGenres)
			}
		}
	}
	return newFilmFromSearched(result.Films[0], c.filmGenres) // Weird, non of the results seems to match what is asked for. Return best guess.
}

func (c *Client) bestShowMatch(title string, result showSearchResult) *Show {
	candidates := result.Shows
	if len(candidates) == 1 || candidates[0].Title == title {
		return newShowFromSearched(candidates[0], c.showGenres)
	}
	if len(candidates) > 1 {
		tuples := make(map[dates.Date]string)
		for _, show := range candidates {
			if _, ok := tuples[show.Premiered]; ok {
				continue // do not overwrite an existing candidate with the same Premiered date.
			}
			tuples[show.Premiered] = cleanTitle(show.Title)
		}
		premiered := selectByStringDistance(title, tuples)
		for _, candidate := range candidates {
			if candidate.Premiered.String() == premiered.String() {
				return newShowFromSearched(candidate, c.showGenres)
			}
		}
	}
	return newShowFromSearched(result.Shows[0], c.showGenres) // Weird, non of the results seems to match what is asked for. Return best guess.
}

func selectByStringDistance(input string, tuples map[dates.Date]string) dates.Date {
	weightmap := make(map[dates.Date]int)
	calc := levenshtein.New(1, 1)
	selectedDate := dates.Date{}
	currentDistance := 0
	for date, text := range tuples {
		distance := calc.Dist(strings.ToLower(text), input)
		if distance > currentDistance {
			currentDistance = distance
		}
		weightmap[date] = distance
	}
	for date, distance := range weightmap {
		if distance < currentDistance || (distance == currentDistance && date.String() > selectedDate.String()) {
			currentDistance = distance
			selectedDate = date
		}
	}
	return selectedDate
}

func cleanTitle(title string) string {
	openBracketPos := strings.Index(title, "(")
	if openBracketPos > -1 {
		closeBracketPos := strings.Index(title, ")")
		if closeBracketPos > -1 {
			title = strings.Trim(title[:openBracketPos], " \n\t")
		}
	}
	return title
}
