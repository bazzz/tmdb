package tmdb

import "gitlab.com/bazzz/dates"

type showSearchResult struct {
	Page  int            `json:"page"`
	Shows []searchedShow `json:"results"`
}

// Show represents a tv show.
type Show struct {
	ID               int        `json:"id"`
	Title            string     `json:"name"`
	Premiered        dates.Date `json:"first_air_date"`
	Overview         string     `json:"overview"`
	OriginalTitle    string     `json:"original_name"`
	OriginCountry    []string   `json:"origin_country"`
	OriginalLanguage string     `json:"original_language"`
	Rating           float64    `json:"vote_average"`
	Popularity       float64    `json:"popularity"`
	Votes            int        `json:"vote_count"`
	Poster           string     `json:"poster_path"`
	Backdrop         string     `json:"backdrop_path"`
	Genres           []string
}

type searchedShow struct {
	Show
	GenreIDs []int `json:"genre_ids"`
}

func newShowFromSearched(input searchedShow, genres map[int]string) *Show {
	s := input.Show
	for _, genreid := range input.GenreIDs {
		genre := genres[genreid]
		if genre != "" {
			s.Genres = append(s.Genres, genre)
		}
	}
	return &s
}

type detailedShow struct {
	Show
	GenreObjects []genre `json:"genres"`
}

func newShowFromDetailed(input detailedShow, genres map[int]string) *Show {
	s := input.Show
	for _, genreObject := range input.GenreObjects {
		genre := genres[genreObject.ID]
		if genre != "" {
			s.Genres = append(s.Genres, genre)
		}
	}
	return &s
}
