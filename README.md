# tmdb

Package tmdb provides a client for the TMDB (The Movie Database) API at http://api.themoviedb.org that can search for Films and TV series episodes. You will need a TVDB API key in order to use this package.

## usage

Simple usage example:

    key := "[my api key]"
    client, err := tmdb.New(key)
    if err != nil {
        log.Print(err)
    }
    film, err := client.SearchFilm("The Matrix", 1999, "")
    if err != nil {
        log.Print(err)
    }
    // Do something with film here.

## installation

You can add package tmdb to your project using a regular import, as it is written in pure Go (no C/C++). Dependencies will be pulled in automatically by `go mod`.
