package tmdb

type genreResult struct {
	Genres []genre `json:"genres"`
}

type genre struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
