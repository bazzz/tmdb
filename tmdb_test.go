package tmdb

import (
	"os"
	"strings"
	"testing"
)

// Make sure there is a file named tmdb.key that contains only your TMDB api key.
const keylocation = "./tmdb.key"

var client *Client

func init() {
	data, err := os.ReadFile(keylocation)
	if err != nil {
		panic("cannot read apikey: " + err.Error())
	}
	if len(data) <= 0 {
		panic("cannot read apikey: file is empty")
	}
	apiKey := string(data)
	c, err := New(apiKey)
	if err != nil {
		panic(err)
	}
	client = c
}

func TestSearchFilmWithoutLanguage(t *testing.T) {
	film, err := client.SearchFilm("Soul", 2020, "")
	if err != nil {
		t.Fatal(err)
	}
	expectedID := 508442
	if film.ID != expectedID {
		t.Fatal("ID", film.ID, "!=", expectedID)
	}
	expectedTitle := "Soul"
	if film.Title != expectedTitle {
		t.Fatal("Title", film.Title, "!=", expectedTitle)
	}
	expectedPremiered := "2020-12-25"
	if film.Premiered.String() != expectedPremiered {
		t.Fatal("Premiered", film.Premiered, "!=", expectedPremiered)
	}
	expectedGenres := "Animation,Family,Comedy"
	if strings.Join(film.Genres, ",") != expectedGenres {
		t.Fatal("Genres", strings.Join(film.Genres, ","), "!=", expectedGenres)
	}
}

func TestSearchFilm(t *testing.T) {
	film, err := client.SearchFilm("Nijntje de Film", 2013, "nl")
	if err != nil {
		t.Fatal(err)
	}
	expectedID := 140248
	if film.ID != expectedID {
		t.Fatal("ID", film.ID, "!=", expectedID)
	}
	expectedTitle := "Nijntje De Film"
	if film.Title != expectedTitle {
		t.Fatal("Title", film.Title, "!=", expectedTitle)
	}
	expectedPremiered := "2013-01-30"
	if film.Premiered.String() != expectedPremiered {
		t.Fatal("Premiered", film.Premiered, "!=", expectedPremiered)
	}
}

func TestGetFilm(t *testing.T) {
	film, err := client.GetFilm(508442, "")
	if err != nil {
		t.Fatal(err)
	}
	expectedTitle := "Soul"
	if film.Title != expectedTitle {
		t.Fatal("Title", film.Title, "!=", expectedTitle)
	}
	expectedPremiered := "2020-12-25"
	if film.Premiered.String() != expectedPremiered {
		t.Fatal("Premiered", film.Premiered, "!=", expectedPremiered)
	}
	expectedGenres := "Animation,Family,Comedy"
	if strings.Join(film.Genres, ",") != expectedGenres {
		t.Fatal("Genres", strings.Join(film.Genres, ","), "!=", expectedGenres)
	}
}

func TestSearchShow(t *testing.T) {
	show, err := client.SearchShow("Scorpion", "")
	if err != nil {
		t.Fatal(err)
	}
	expectedID := 60797
	if show.ID != expectedID {
		t.Fatal("ID", show.ID, "!=", expectedID)
	}
}

func TestGetShow(t *testing.T) {
	show, err := client.GetShow(37290, "it")
	if err != nil {
		t.Fatal(err)
	}
	expectedTitle := "Il Commissario Montalbano"
	if show.Title != expectedTitle {
		t.Fatal("Title", show.Title, "!=", expectedTitle)
	}
	expectedLanguage := "it"
	if show.OriginalLanguage != expectedLanguage {
		t.Fatal("OriginalLanguage", show.OriginalLanguage, "!=", expectedLanguage)
	}
	expectedGenres := "Action & Adventure,Crime,Mystery,Drama"
	if strings.Join(show.Genres, ",") != expectedGenres {
		t.Fatal("Genres", strings.Join(show.Genres, ","), "!=", expectedGenres)
	}
}

func TestGetEpisode(t *testing.T) {
	episode, err := client.GetEpisode(37290, 1, 1, "it")
	if err != nil {
		t.Fatal(err)
	}
	expectedID := 843940
	if episode.ID != expectedID {
		t.Fatal("ID", episode.ID, "!=", expectedID)
	}
	expectedTitle := "Il ladro di merendine"
	if episode.Title != expectedTitle {
		t.Fatal("Title", episode.Title, "!=", expectedTitle)
	}
	expectedPremiered := "1999-05-06"
	if episode.Premiered.String() != expectedPremiered {
		t.Fatal("Premiered", episode.Premiered, "!=", expectedPremiered)
	}
}

func TestSearchShowWithCommonName(t *testing.T) {
	show, err := client.SearchShow("Heidi", "it")
	if err != nil {
		t.Fatal(err)
	}
	expectedID := 62475
	if show.ID != expectedID {
		t.Fatal("ID", show.ID, "!=", expectedID)
	}
}
