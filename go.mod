module gitlab.com/bazzz/tmdb

go 1.16

require (
	github.com/ferhatelmas/levenshtein v0.0.0-20160518143259-a12aecc52d76
	gitlab.com/bazzz/dates v0.0.0-20211008090510-94618edb9094
)
